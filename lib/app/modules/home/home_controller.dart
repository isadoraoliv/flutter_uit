import 'package:flutter_uit/app/modules/home/models/post_model.dart';
import 'package:flutter_uit/app/modules/home/repositories/home_repository.dart';
import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final HomeRepository repo;

  _HomeControllerBase(this.repo);

  @observable
  List<PostModel> listPosts = <PostModel>[];

  @action
  Future fetchPosts() async {
    listPosts = await repo.getPosts();
  }
}
