// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$listPostsAtom = Atom(name: '_HomeControllerBase.listPosts');

  @override
  List<PostModel> get listPosts {
    _$listPostsAtom.reportRead();
    return super.listPosts;
  }

  @override
  set listPosts(List<PostModel> value) {
    _$listPostsAtom.reportWrite(value, super.listPosts, () {
      super.listPosts = value;
    });
  }

  final _$fetchPostsAsyncAction = AsyncAction('_HomeControllerBase.fetchPosts');

  @override
  Future<dynamic> fetchPosts() {
    return _$fetchPostsAsyncAction.run(() => super.fetchPosts());
  }

  @override
  String toString() {
    return '''
listPosts: ${listPosts}
    ''';
  }
}
