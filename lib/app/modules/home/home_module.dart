import 'package:dio/dio.dart';
import 'package:flutter_uit/app/modules/home/home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_uit/app/modules/home/home_page.dart';
import 'package:flutter_uit/app/modules/home/repositories/home_repository.dart';
import 'package:flutter_uit/app/modules/home/repositories/home_repository_impl.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeRepositoryImpl(i.get<Dio>())),
        Bind((i) => HomeController(i.get<HomeRepository>())),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => HomePage()),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
