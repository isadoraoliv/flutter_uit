import 'package:dio/dio.dart';
import 'package:flutter_uit/app/modules/home/models/post_model.dart';
import 'package:flutter_uit/app/modules/home/repositories/home_repository.dart';

class HomeRepositoryImpl implements HomeRepository {
  final Dio _client;

  HomeRepositoryImpl(this._client);

  @override
  Future getPosts() async {
    final response =
        await _client.get('https://jsonplaceholder.typicode.com/photos');
    return (response.data as List).map((i) => PostModel.fromJson(i)).toList();
  }
}
