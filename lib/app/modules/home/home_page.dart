import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_uit/app/modules/home/repositories/home_repository.dart';
import 'package:flutter_uit/app/modules/home/widgets/custom_list.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  HomeRepository repo;

  @override
  void initState() {
    controller.fetchPosts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Observer(
        builder: (_) {
          if (controller.listPosts.length != null) {
            return ListView.builder(
              itemCount: controller.listPosts.length,
              itemBuilder: (context, index) {
                return CustomList(
                    text: controller.listPosts[index].title,
                    url: controller.listPosts[index].url);
              },
            );
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
