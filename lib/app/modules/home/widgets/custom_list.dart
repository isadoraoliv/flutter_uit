import 'package:flutter/material.dart';

class CustomList extends StatelessWidget {
  final String text;
  final String url;

  const CustomList({
    Key key,
    @required this.text,
    @required this.url,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          ListTile(
            title: Text(text),
            leading: ClipOval(
              child: Image(
                image: NetworkImage(url),
              ),
            ),
          ),
          Divider(
            color: Colors.grey,
            thickness: 1,
            indent: MediaQuery.of(context).size.width * 0.2,
          )
        ],
      ),
    );
  }
}
